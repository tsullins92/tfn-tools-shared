export class Instrument {
	id: number;
	name: string;
	address: string;
	type:string;
	port: number;
}
